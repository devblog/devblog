terraform {
    backend "s3" {
        region = "us-east-1"
        bucket = "pbecotte-tf-state"
        key = "devblog.frontend.tfstate"
    }
}

data "aws_eks_cluster" "this" {
    name = "devblog"
}

data "aws_eks_cluster_auth" "this" {
    name = "devblog"
}

provider "kubernetes" {
    host                   = data.aws_eks_cluster.this.endpoint
    cluster_ca_certificate = base64decode(data.aws_eks_cluster.this.certificate_authority[0].data)
    token                  = data.aws_eks_cluster_auth.this.token
}
