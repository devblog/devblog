resource "kubernetes_deployment" "react" {
    metadata {
        name = "frontend"
        namespace = "devblog"
    }
    spec {
        replicas = 2
        revision_history_limit = 2
        selector {
            match_labels = {
              "app.kubernetes.io/name": "frontend"
            }
        }
        template {
            metadata {
                labels = {
                    "app.kubernetes.io/name": "frontend"
                    "log_type": "nginx"
                }
            }
            spec {
                container {
                    name = "react"
                    image = "registry.gitlab.com/devblog/devblog/react:${var.IMAGE_TAG}"
                    liveness_probe {
                        http_get {
                            path = "/health"
                            port = "http"
                            scheme = "HTTP"
                        }
                        period_seconds = 10
                        success_threshold = 1
                        timeout_seconds = 1
                        failure_threshold = 3
                    }
                    readiness_probe {
                        http_get {
                            path = "/health"
                            port = "http"
                            scheme = "HTTP"
                        }
                        period_seconds = 10
                        success_threshold = 1
                        timeout_seconds = 1
                        failure_threshold = 3
                    }
                    port {
                        container_port = 80
                        name = "http"
                        protocol = "TCP"
                    }
                    resources {
                        requests = {
                            cpu = "100m"
                            memory = "128Mi"
                        }
                        limits = {
                            cpu = "100m"
                            memory = "128Mi"
                        }
                    }
                }
                node_selector = {
                    "kubernetes.io/arch" = "amd64"
                }
            }
        }
    }
}

resource "kubernetes_service" "frontend" {
    metadata {
        name = "frontend"
        namespace = "devblog"
    }
    spec {
        selector = {
            "app.kubernetes.io/name": "frontend"
        }
        port {
            port = 80
            name = "http"
            protocol = "TCP"
            target_port = "http"
        }
        type = "ClusterIP"
    }
}

resource "kubernetes_ingress_v1" "frontend" {
    metadata {
        name = "frontend"
        namespace = "devblog"
        annotations = {
            "kubernetes.io/ingress.class" = "nginx"
        }
    }
    spec {
        rule {
            host = "paulbecotte.com"
            http {
                path {
                    path = "/"
                    backend {
                        service {
                            name = "frontend"
                            port {
                                name = "http"
                            }
                        }
                    }
                }
            }
        }
    }
}