VERSION --try --run-with-aws --run-with-aws-oidc 0.8

pnpm:
    FROM node:20-slim
    ENV PNPM_HOME="/pnpm"
    ENV PATH="$PNPM_HOME:$PATH"
    RUN corepack enable
    WORKDIR /app
    COPY .npmrc package.json pnpm-lock.yaml .

lock:
    FROM +pnpm
    RUN --mount=type=cache,id=pnpm,target=/pnpm/store pnpm install
    SAVE ARTIFACT --force pnpm-lock.yaml AS LOCAL pnpm-lock.yaml

app-base:
    FROM +pnpm
    RUN --mount=type=cache,id=pnpm,target=/pnpm/store pnpm install --frozen-lockfile
    COPY --dir src public .
    ENV REACT_APP_API_URL https://paulbecotte.com/api

    RUN pnpm run build
    SAVE ARTIFACT /app/build

build:
    FROM nginx:latest
    COPY default.conf /etc/nginx/conf.d/default.conf
    COPY +app-base/build /usr/share/nginx/html/
    ARG EARTHLY_GIT_HASH
    SAVE IMAGE react:$EARTHLY_GIT_HASH

publish:
    FROM +build
    ARG --required IMAGE_TAG
    SAVE IMAGE --push registry.gitlab.com/devblog/devblog/react:$IMAGE_TAG

terraform-init:
    FROM hashicorp/terraform:light
    COPY deploy .
    ENV AWS_REGION=us-east-1
    RUN --aws terraform init --upgrade

plan:
    FROM +terraform-init
    ARG --required IMAGE_TAG
    RUN --aws TF_VAR_IMAGE_TAG=$IMAGE_TAG terraform plan

deploy:
    FROM +terraform-init
    ARG --required IMAGE_TAG
    RUN --aws --push TF_VAR_IMAGE_TAG=$IMAGE_TAG terraform apply -auto-approve
