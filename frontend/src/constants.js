const apiUrlVar = process.env.REACT_APP_API_URL;
export const apiUrl = apiUrlVar ? apiUrlVar : "http://localhost:5000/api";
