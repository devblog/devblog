from functools import wraps
from typing import Optional

from flask import Flask, jsonify, request
from pockets.autolog import log
from sqlalchemy.orm.exc import NoResultFound


class ApiException(Exception):
    status_code = 500
    message = "an error occurred"
    name = "Error"

    def __init__(
        self, message: Optional[str] = None, status_code: Optional[int] = None
    ):
        super().__init__(self, message)
        log.exception(self)

        if message:
            self.message = message
        if status_code:
            self.status_code = status_code

    def to_dict(self):
        return {"message": self.message, "type": self.name}


class ApiError(ApiException):
    status_code = 400
    name = "Bad Request"


class Unauthorized(ApiException):
    status_code = 401
    name = "Unauthorized"


class Forbidden(ApiException):
    status_code = 403
    name = "Forbidden"


class NotFound(ApiException):
    status_code = 404
    name = "Not Found"


def make_response(error: Optional[ApiException] = None, status_code=None, **kwargs):
    status = status_code or error.status_code if error else 200
    return (
        jsonify({"data": kwargs, "error": error.to_dict() if error else {}}),
        status,
    )


class ApiExceptions:
    def __init__(self, app: Optional[Flask] = None):
        if app:
            self.register_app(app)

    @staticmethod
    def register_app(app: Flask):
        @app.errorhandler(ApiException)
        def _special_exception_handler(exc: ApiException):
            return (
                jsonify({"data": {}, "error": {"message": exc.message}}),
                exc.status_code,
            )

        @app.errorhandler(Exception)
        def _uncaught_handler(exc: Exception):
            log.exception(exc)
            return (
                jsonify({"data": {}, "error": {"message": "uncaught exception"}}),
                500,
            )


def enforce_json(view):
    @wraps(view)
    def wrapper(*args, **kwargs):
        if not request.is_json:
            raise ApiError("Request must be JSON")
        return view(*args, **kwargs)

    return wrapper


def get_or_404(query):
    try:
        return query.one()
    except NoResultFound as e:
        raise NotFound("Could not find object") from e
