from datetime import datetime

from flask import Blueprint, request
from flask_jwt_extended import current_user, get_jwt_identity, jwt_required
from marshmallow import EXCLUDE, Schema, fields

from devblog.api_security import User, admin_required, load_token_if_current
from devblog.db import Session
from devblog.posts.images import ImageSchema, upload_image
from devblog.posts.models import Comment, Image, Post
from devblog.posts.posts import (
    PostDetailSchema,
    PostStubSchema,
    get_post_detail,
    get_post_list,
)
from devblog.responses import ApiError, get_or_404, make_response

POST_API = Blueprint("post_api", __name__)


class CommentSchema(Schema):
    id = fields.Integer(dump_only=True)
    username = fields.Str(attribute="user.alias", dump_only=True)
    text = fields.Str()
    timestamp = fields.DateTime(dump_only=True)


@POST_API.route("/post")
@load_token_if_current
def api_index():
    entries = get_post_list()
    data = PostStubSchema(many=True).dump(entries)
    return make_response(posts=data)


@POST_API.route("/post", methods=["POST"])
@admin_required
def api_create_post():
    current_id = get_jwt_identity()
    post_data = PostDetailSchema().load(request.json["post"], unknown=EXCLUDE)
    session = Session()
    author = session.query(User).filter(User.email == current_id).one()
    image = post_data.pop("header_image", None) or {}
    images = post_data.pop("images", None) or []
    post = Post(header_image_id=image.get("id"), author=author, **post_data)
    for img in images:
        img_object = session.query(Image).get(img["id"])
        img_object.post = post
    session.add(post)
    session.commit()
    post = PostDetailSchema().dump(post)
    return make_response(post=post)


@POST_API.route("/post/<slug>", methods=["GET", "PUT", "DELETE"])
@load_token_if_current
def api_post_detail(slug):
    post_model = get_post_detail(slug)
    post = PostDetailSchema().dump(post_model)
    return make_response(post=post)


@POST_API.route("/comment/<slug>")
def fetch_comments(slug):
    q = Session().query(Post).filter(Post.slug == slug)
    post = get_or_404(q)
    data = CommentSchema(many=True).dump(post.comments)
    return make_response(comments=data)


@POST_API.route("/comment/<slug>", methods=["POST"])
@jwt_required()
def add_comment(slug):
    comment_dict = CommentSchema().load(request.json, unknown=EXCLUDE)
    q = Session().query(Post).filter(Post.slug == slug)
    post = get_or_404(q)
    post.comments.append(
        Comment(
            text=comment_dict["text"],
            user=current_user,
            timestamp=datetime.utcnow(),
        )
    )
    Session().commit()
    data = CommentSchema(many=True).dump(post.comments)
    return make_response(comments=data)


@POST_API.route("/image", methods=["POST"])
@admin_required
def post_image():
    # check if the post request has the file part
    if "file" not in request.files:
        raise ApiError("No file uploaded")
    file = request.files["file"]
    url = upload_image(file)
    image = Image(url=url)
    session = Session()
    session.add(image)
    session.commit()
    image_data = ImageSchema().dump(image)
    return make_response(image=image_data)
