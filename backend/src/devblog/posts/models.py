import re
from datetime import datetime

from sqlalchemy import Column, ForeignKey, String, Text
from sqlalchemy.ext.hybrid import hybrid_property
from sqlalchemy.orm import Mapped, mapped_column, relationship

from devblog.api_security.models import User
from devblog.db import Base


class Post(Base):
    __tablename__ = "post"
    id: Mapped[int] = mapped_column(primary_key=True)
    author_id: Mapped[int] = mapped_column(ForeignKey("user.id"))
    author: Mapped[User] = relationship("User")
    _title = Column("title", String(255), unique=True)
    _slug = Column("slug", String(255), unique=True)
    tagline: Mapped[str]
    content: Mapped[str]
    published: Mapped[bool]
    timestamp: Mapped[datetime] = mapped_column(default=datetime.now)
    header_image_id: Mapped[int] = mapped_column(ForeignKey("image.id"))
    header_image: Mapped["Image"] = relationship(
        "Image", foreign_keys=[header_image_id]
    )
    images: Mapped[list["Image"]] = relationship("Image", foreign_keys="Image.post_id")
    comments: Mapped[list["Comment"]] = relationship(
        "Comment", order_by="Comment.timestamp", back_populates="post"
    )

    @hybrid_property
    def title(self):
        return self._title

    @title.setter  # type: ignore
    def title(self, value):
        if not self.slug:
            self._slug = re.sub(r"[^\w]+", "-", value.lower())
        self._title = value

    @hybrid_property
    def slug(self):
        return self._slug

    @slug.setter  # type: ignore
    def slug(self, value):
        raise AttributeError("This property is read only")


class Image(Base):
    __tablename__ = "image"
    id: Mapped[int] = mapped_column(primary_key=True)
    url: Mapped[str]
    post_id: Mapped[int | None] = mapped_column(ForeignKey("post.id"), nullable=True)
    post: Mapped[Post | None] = relationship(
        "Post", foreign_keys=post_id, back_populates="images"
    )


class Comment(Base):
    __tablename__ = "comment"
    id: Mapped[int] = mapped_column(primary_key=True)
    user_id: Mapped[int] = mapped_column(ForeignKey("user.id"))
    user: Mapped[User] = relationship("User")
    post_id: Mapped[int] = mapped_column(ForeignKey("post.id"))
    post: Mapped[Post] = relationship("Post", back_populates="comments")
    text: Mapped[str | None] = mapped_column(Text, nullable=True)
    timestamp: Mapped[datetime]
