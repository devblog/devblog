import os
from uuid import uuid4

from flask import current_app
from marshmallow import Schema, fields
from werkzeug.utils import secure_filename


def upload_image(image, acl="public-read"):
    filename = secure_filename(image.filename)
    source_extension = os.path.splitext(filename)[1]
    destination_filename = uuid4().hex + source_extension
    key = "/".join([current_app.config["S3_UPLOAD_DIRECTORY"], destination_filename])
    bucket = current_app.config["S3_BUCKET"]
    current_app.devblog.s3.put_object(
        Body=image.stream.read(), Bucket=bucket, Key=key, ACL=acl
    )
    url = f"https://{bucket}.s3.amazonaws.com/{key}"
    return url


class ImageSchema(Schema):
    id = fields.Integer(required=False)
    url = fields.Str()
