from opentelemetry.instrumentation.sqlalchemy import SQLAlchemyInstrumentor
from sqlalchemy import create_engine, event
from sqlalchemy.engine import Engine
from sqlalchemy.orm import declarative_base, scoped_session, sessionmaker

from devblog import config


class SessionFactory:
    _engine: Engine
    _sessionmaker: sessionmaker

    @property
    def engine(self):
        if not hasattr(self, "_engine"):
            self._engine = create_engine(
                config.SQLALCHEMY_DATABASE_URI,
                connect_args=config.SQLALCHEMY_CONNECT_ARGS,
            )
            SQLAlchemyInstrumentor().instrument(engine=self._engine)

            @event.listens_for(self._engine, "connect")
            def do_connect(
                dbapi_connection, connection_record
            ):  # pylint: disable=unused-argument
                # disable pysqlite's emitting of the BEGIN statement entirely.
                # also stops it from emitting COMMIT before any DDL.
                dbapi_connection.isolation_level = None

            @event.listens_for(self._engine, "begin")
            def do_begin(_conn):
                _conn.exec_driver_sql("BEGIN")

        return self._engine

    @property
    def sessionmaker(self):
        if not hasattr(self, "_sessionmaker"):
            self._sessionmaker = sessionmaker(bind=self.engine)
        return self._sessionmaker

    def __call__(self, *args, **kwargs):
        return self.sessionmaker(*args, **kwargs)


Session = scoped_session(SessionFactory())
Base = declarative_base()
