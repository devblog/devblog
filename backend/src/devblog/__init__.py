import logging
import os

LEVEL = os.environ.get("LOG_LEVEL", "INFO")
fmt = "%(asctime)s %(levelname)s [%(name)s] %(message)s"
logging.basicConfig(format=fmt, level=getattr(logging, LEVEL))
