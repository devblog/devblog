from functools import wraps

import click
from flask import Flask
from flask_jwt_extended import JWTManager, get_jwt, verify_jwt_in_request
from flask_jwt_extended.exceptions import UserClaimsVerificationError
from jwt import ExpiredSignatureError
from pockets.autolog import log
from sqlalchemy import select

from devblog.api_security.models import Role, User
from devblog.api_security.views import SECURITY_API
from devblog.db import Session
from devblog.responses import Forbidden


class FlaskTokens:
    def __init__(self, app=None):
        if app:
            self.register_app(app)

    def register_app(self, app: Flask):
        jwt = JWTManager(app)
        app.register_blueprint(SECURITY_API)

        @jwt.additional_claims_loader
        def _add_claims_to_access_token(user):
            return {"roles": [role.name for role in user.roles]}

        @jwt.user_identity_loader
        def _user_identity_lookup(user):
            return user.email

        @jwt.user_lookup_loader
        def _user_loader_callback(
            jwt_header, jwt_payload
        ):  # pylint: disable=unused-argument
            q = select(User).where(User.email == jwt_payload["sub"])
            return Session().execute(q).scalar()

        app.flask_tokens = self  # type: ignore

        @app.cli.command("create-user")
        @click.argument("alias")
        @click.argument("email")
        @click.argument("password")
        def _create_user(alias, email, password):
            log.info("creating user")
            q = select(Role).where(Role.name == "admin")
            role = Session().execute(q).scalar() or Role(name="admin")
            user = User(alias=alias, email=email, password=password, roles=[role])
            Session().add_all([role, user])
            Session().commit()


def admin_required(fn):
    @wraps(fn)
    def wrapper(*args, **kwargs):
        verify_jwt_in_request()
        claims = get_jwt()
        if "admin" not in claims["roles"]:
            raise Forbidden("Not authorized for this endpoint")
        return fn(*args, **kwargs)

    return wrapper


def load_token_if_current(fn):
    @wraps(fn)
    def wrapper(*args, **kwargs):
        try:
            verify_jwt_in_request(optional=True)
        except (ExpiredSignatureError, UserClaimsVerificationError):
            pass
        return fn(*args, **kwargs)

    return wrapper
