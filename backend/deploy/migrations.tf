resource "kubernetes_job" "migration" {
    wait_for_completion = true
    metadata {
        name = "devblog-migration"
        namespace = "devblog"
    }
    spec {
        backoff_limit = 0
        template {
            metadata {}
            spec {
                restart_policy = "Never"
                container {
                    name = "alembic-migration"
                    image = "registry.gitlab.com/devblog/devblog/flask:${var.IMAGE_TAG}"
                    command = [
                        "alembic"
                    ]
                    args = [
                        "upgrade",
                        "head",
                    ]
                    env {
                        name = "POSTGRES_HOST"
                        value = "postgres"
                    }
                    env {
                        name = "POSTGRES_PASSWORD"
                        value_from {
                            secret_key_ref {
                                name = kubernetes_secret.postgres.metadata.0.name
                                key = "postgres_password.txt"
                            }
                        }
                    }
                }
                init_container {
                    name = "check-db-ready"
                    image = local.pg_image
                    command = [
                        "sh",
                        "-c",
                        <<-EOT
                            until pg_isready -h postgres -p 5432; do
                                echo waiting for database
                                sleep 2
                            done
                        EOT
                    ]
                    env {
                        name = "POSTGRES_HOST"
                        value = "postgres"
                    }
                    env {
                        name = "POSTGRES_PASSWORD"
                        value_from {
                            secret_key_ref {
                                name = kubernetes_secret.postgres.metadata.0.name
                                key = "postgres_password.txt"
                            }
                        }
                    }
                }

                node_selector = {
                    "kubernetes.io/arch" = "amd64"
                }
            }
        }
    }
}
