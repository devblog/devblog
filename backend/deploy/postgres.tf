terraform {
    required_providers {
        kubernetes = {
            source  = "hashicorp/kubernetes"
            version = "2.31.0"
        }
    }
}
locals {
    pg_image = "postgres:11.5"
}

resource "random_password" "postgres" {
  length           = 12
  special          = false
}

resource "kubernetes_secret" "postgres" {
    metadata {
        name = "postgres-password"
        namespace = "devblog"
    }
    data = {
        "postgres_password.txt" = random_password.postgres.result
    }
}

resource "kubernetes_secret" "postgres_grafana" {
    metadata {
        name = "postgres-password"
        namespace = "alloy"
    }
    data = {
        "password" = random_password.postgres.result
    }
}

resource "kubernetes_persistent_volume" "postgres" {
    metadata {
        name = "blah"
    }
    spec {
        access_modes = ["ReadWriteOnce"]
        capacity = {
            storage = "30Gi"
        }
        claim_ref {
            name = "postgres-data-postgres-0"
            namespace = "devblog"
        }
        persistent_volume_reclaim_policy = "Retain"
        storage_class_name = "auto-ebs-sc"
        volume_mode = "Filesystem"
        node_affinity {
            required {
                node_selector_term {
                    match_expressions {
                        key      = "topology.kubernetes.io/zone"
                        operator = "In"
                        values   = ["us-east-1b"]
                    }
                }
            }
        }
        persistent_volume_source {
            csi {
                driver            = "ebs.csi.eks.amazonaws.com"
                fs_type           = "ext4"
                volume_handle     = "vol-05b1bf48e4e373eab"
            }
        }
    }
}

resource "kubernetes_stateful_set" "postgres" {
    metadata {
        name = "postgres"
        namespace = "devblog"
    }
    spec {
        service_name = "postgres"
        replicas = "1"
        selector {
            match_labels = {
                svc = "postgres"
            }
        }
        template {
            metadata {
                annotations = {
                    "k8s.grafana.com/logs.job" = "integrations/postgres_exporter"
                }
                labels = {
                    svc = "postgres"
                }
            }
            spec {
                container {
                    name = "postgres"
                    image = local.pg_image
                    args = [
                        "-c",
                        "config_file=/etc/postgres.conf",
                        "-c",
                        "hba_file=/etc/pg_hba.conf",
                    ]
                    liveness_probe {
                        exec {
                            command = ["sh", "-c", "exec pg_isready --host $POD_IP"]
                        }
                        failure_threshold = "6"
                        initial_delay_seconds = "60"
                        period_seconds = "10"
                        success_threshold = "1"
                        timeout_seconds = "5"
                    }
                    readiness_probe {
                        exec {
                            command = ["sh", "-c", "exec pg_isready --host $POD_IP"]
                        }
                        failure_threshold = "3"
                        initial_delay_seconds = "5"
                        period_seconds = "5"
                        success_threshold = "1"
                        timeout_seconds = "3"
                    }
                    resources {
                        requests = {
                            cpu = "100m"
                            memory = "256Mi"
                        }
                    }
                    port {
                        container_port = 5432
                        name = "postgres"
                        protocol = "TCP"
                    }
                    env {
                        name = "POSTGRES_USER"
                        value = "postgres"
                    }
                    env {
                        name = "PGUSER"
                        value = "postgres"
                    }
                    env {
                        name = "POSTGRES_DB"
                        value = "postgres"
                    }
                    env {
                        name = "PGDATA"
                        value = "/var/lib/postgresql/data/pgdata"
                    }
                    env {
                        name = "POSTGRES_PASSWORD"
                        value_from {
                            secret_key_ref {
                                name = kubernetes_secret.postgres.metadata.0.name
                                key = "postgres_password.txt"
                            }
                        }
                    }
                    env {
                        name = "POD_IP"
                        value_from {
                            field_ref {
                                api_version = "v1"
                                field_path = "status.podIP"
                            }
                        }
                    }
                    volume_mount {
                        mount_path = "/var/lib/postgresql/data/pgdata"
                        name       = "postgres-data"
                        sub_path = "postgres-db"
                    }
                    volume_mount {
                        mount_path = "/etc/postgres.conf"
                        name       = "postgres-config"
                        sub_path = "postgres.conf"
                    }
                    volume_mount {
                        mount_path = "/etc/pg_hba.conf"
                        name       = "postgres-config"
                        sub_path = "pg_hba.conf"
                    }
                    termination_message_path = "/dev/termination-log"
                    termination_message_policy = "File"
                }
                init_container {
                    name = "postgres-init"
                    image = local.pg_image
                    command = [
                        "/bin/sh",
                        "-c",
                        "psql -c \"ALTER USER postgres WITH PASSWORD ''\" || true"
                    ]
                    env {
                        name = "POSTGRES_USER"
                        value = "postgres"
                    }
                    env {
                        name = "PGUSER"
                        value = "postgres"
                    }
                    env {
                        name = "POSTGRES_DB"
                        value = "postgres"
                    }
                    env {
                        name = "PGDATA"
                        value = "/var/lib/postgresql/data/pgdata"
                    }
                    env {
                        name = "POSTGRES_PASSWORD"
                        value_from {
                            secret_key_ref {
                                name = kubernetes_secret.postgres.metadata.0.name
                                key = "postgres_password.txt"
                            }
                        }
                    }
                    volume_mount {
                        mount_path = "/var/lib/postgresql/data/pgdata"
                        name       = "postgres-data"
                        sub_path = "postgres-db"
                    }
                    volume_mount {
                        mount_path = "/etc/postgres.conf"
                        name       = "postgres-config"
                        sub_path = "postgres.conf"
                    }
                    volume_mount {
                        mount_path = "/etc/pg_hba.conf"
                        name       = "postgres-config"
                        sub_path = "pg_hba.conf"
                    }
                    termination_message_path = "/dev/termination-log"
                    termination_message_policy = "File"
                }
                termination_grace_period_seconds = "10"
                volume {
                    name = "postgres-config"
                    config_map {
                        name = "postgres"
                    }
                }
            }
        }
        volume_claim_template {
            metadata {
                name = "postgres-data"
            }
            spec {
                access_modes = ["ReadWriteOnce"]
                storage_class_name = "auto-ebs-sc"
                resources {
                    requests = {
                        storage = "30Gi"
                    }
                }
            }
        }
    }
}

resource "kubernetes_service" "postgres" {
    metadata {
        name = "postgres"
        namespace = "devblog"
    }
    spec {
        port {
            name = "postgres"
            port = "5432"
            protocol = "TCP"
            target_port = "postgres"
        }
        selector = {
            svc = "postgres"
        }
        type = "ClusterIP"
        session_affinity = "None"
    }
}

resource "kubernetes_config_map" "postgres" {
    metadata {
        name = "postgres"
        namespace = "devblog"
    }
    data = {
        "postgres.conf" = file("${path.module}/postgres.conf")
        "pg_hba.conf" = file("${path.module}/pg_hba.conf")
    }
}

resource "kubernetes_stateful_set" "postgres_exporter" {
    metadata {
        name = "postgres-exporter"
        namespace = "devblog"
    }
    spec {
        service_name = "postgres-exporter"
        replicas = "1"
        selector {
            match_labels = {
                svc = "postgres-exporter"
            }
        }
        template {
            metadata {
                annotations = {
                    "k8s.grafana.com/scrape" = "true"
                    "k8s.grafana.com/job" = "integrations/postgres_exporter"
                    "k8s.grafana.com/logs.job" = "integrations/postgres_exporter"
                    "k8s.grafana.com/metrics.portNumber" = "9187"
                }
                labels = {
                    svc = "postgres-exporter"
                }
            }
            spec {
                container {
                    name = "exporter"
                    image = "bitnami/postgres-exporter:latest"
                    resources {
                        requests = {
                            cpu = "10m"
                            memory = "40Mi"
                        }
                    }
                    port {
                        container_port = 9187
                        name = "exporter"
                        protocol = "TCP"
                    }
                    env {
                        name = "DATA_SOURCE_URI"
                        value = "postgres.devblog.svc.cluster.local:5432/postgres?sslmode=disable"
                    }
                    env {
                        name = "DATA_SOURCE_USER"
                        value = "postgres"
                    }
                    env {
                        name = "DATA_SOURCE_PASS"
                        value_from {
                            secret_key_ref {
                                name = kubernetes_secret.postgres.metadata.0.name
                                key = "postgres_password.txt"
                            }
                        }
                    }
                }
            }
        }
    }
}