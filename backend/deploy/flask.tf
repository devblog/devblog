resource "kubernetes_deployment" "backend" {
    depends_on = [
        kubernetes_job.migration
    ]
    metadata {
        name = "backend"
        namespace = "devblog"
    }
    spec {
        replicas = "2"
        revision_history_limit = 3
        selector {
            match_labels = {
              "app.kubernetes.io/name" = "backend"
            }
        }
        template {
            metadata {
                labels = {
                    "app.kubernetes.io/name" = "backend"
                    "resources_service_name" = "devblog-backend"
                }
            }
            spec {
                service_account_name = "backend"
                container {
                    name = "devblog"
                    image = "registry.gitlab.com/devblog/devblog/flask:${var.IMAGE_TAG}"
                    command = [
                        "gunicorn",
                    ]
                    args = [
                       "-c",
                       "python:devblog.gunicorn_conf",
                       "devblog.uwsgi:app",
                    ]
                    port {
                        container_port = 5000
                        name = "http"
                        protocol = "TCP"
                    }
                    env {
                        name = "K8S_HOST_IP"
                        value = "alloy.alloy.svc.cluster.local"
                    }
                    env {
                        name = "POSTGRES_HOST"
                        value = "postgres"
                    }
                    env {
                        name = "POSTGRES_PASSWORD"
                        value_from {
                            secret_key_ref {
                                name = kubernetes_secret.postgres.metadata.0.name
                                key = "postgres_password.txt"
                            }
                        }
                    }
                    env {
                        name = "ENV"
                        value = "release"
                    }
                    volume_mount {
                        mount_path = "/keys"
                        name       = "jwt-key"
                        read_only = true
                    }
                    resources {
                        limits = {
                            memory = "500Mi"
                        }
                        requests = {
                            cpu = "250m"
                            memory = "125Mi"
                        }
                    }
                    liveness_probe {
                        http_get {
                            path = "/health"
                            port = "http"
                            scheme = "HTTP"
                        }
                        initial_delay_seconds = 10
                        period_seconds = 10
                        success_threshold = 1
                        timeout_seconds = 1
                    }
                }
                volume {
                    name = "jwt-key"
                    secret {
                        secret_name = kubernetes_secret.jwt.metadata.0.name
                    }
                }
                node_selector = {
                    "kubernetes.io/arch" = "amd64"
                }
            }
        }
    }
}

resource "kubernetes_service" "backend" {
    metadata {
        name = "backend"
        namespace = "devblog"
    }
    spec {
        selector = {
            "app.kubernetes.io/name": "backend"
        }
        port {
            port = 5000
            name = "http"
            protocol = "TCP"
            target_port = "http"
        }
        port {
            port = 9100
            name = "metrics"
            protocol = "TCP"
            target_port = "metrics"
        }
        type = "ClusterIP"
    }
}

resource "kubernetes_ingress_v1" "backend" {
    metadata {
        name = "backend"
        namespace = "devblog"
        annotations = {
            "kubernetes.io/ingress.class" = "nginx"
        }
    }
    spec {
        rule {
            host = "paulbecotte.com"
            http {
                path {
                    path = "/api/"
                    backend {
                        service {
                            name = kubernetes_service.backend.metadata.0.name
                            port {
                                name = "http"
                            }
                        }
                    }
                }
            }
        }
    }
}

resource "tls_private_key" "jwt" {
    algorithm = "RSA"
    rsa_bits = 2048
}

resource "kubernetes_secret" "jwt" {
    metadata {
        name = "jwt-key"
        namespace = "devblog"
    }
    data = {
        "private_key.pem": tls_private_key.jwt.private_key_pem
        "public_key.pem": tls_private_key.jwt.public_key_pem
    }
}

data "aws_iam_policy_document" "backend" {
  statement {
    actions = [
      "s3:*",
    ]

    resources = [
      "arn:aws:s3:::paulbecotte-images",
      "arn:aws:s3:::paulbecotte-images/*",
    ]
  }
}

resource "kubernetes_service_account" "backend" {
    metadata {
        name = "backend"
        namespace = "devblog"
        annotations = {
            "eks.amazonaws.com/role-arn": module.backend_iam_role.iam_role_arn
        }
    }
}

resource "aws_iam_policy" "backend" {
    name = "devblog_backend"
    path = "/"
    policy = data.aws_iam_policy_document.backend.json
}

module "backend_iam_role" {
  source    = "terraform-aws-modules/iam/aws//modules/iam-role-for-service-accounts-eks"

  role_name = "devblog_backend"
  role_policy_arns = {
    "s3" = aws_iam_policy.backend.arn
  }
  oidc_providers = {
    main = {
      provider_arn               = data.aws_iam_openid_connect_provider.cluster.arn
      namespace_service_accounts = ["devblog:backend"]
    }
  }
}
