"""initial migration

Revision ID: 3448071662dc
Revises: 
Create Date: 2015-05-28 20:49:57.657797

"""

# revision identifiers, used by Alembic.
revision = '3448071662dc'
down_revision = None
branch_labels = None
depends_on = None

from alembic import op
import sqlalchemy as sa


def upgrade():
    op.create_table(
        'role',
        sa.Column('id', sa.Integer(), primary_key=True),
        sa.Column('name', sa.String(length=80)),
        sa.Column('description', sa.String(length=255)),
        sa.PrimaryKeyConstraint('id', name='role_primary_key'),
        sa.UniqueConstraint('name', name='name_unique_constraint'),
    )
    op.create_table(
        'user',
        sa.Column('id', sa.Integer(), primary_key=True),
        sa.Column('email', sa.String(length=255), nullable=False),
        sa.Column('password', sa.String(length=255), nullable=False),
        sa.Column('active', sa.Boolean(), nullable=False),
        sa.Column('confirmed_at', sa.DateTime()),
        sa.Column('alias', sa.String(length=255), nullable=False, unique=True),
        sa.PrimaryKeyConstraint('id', name='user_primary_key'),
        sa.UniqueConstraint('email', name='email_unique_constraint'),
    )
    op.create_table(
        'roles_user',
        sa.Column('user_id', sa.Integer(), primary_key=True),
        sa.Column('role_id', sa.Integer(), primary_key=True),
        sa.ForeignKeyConstraint(['role_id'], ['role.id'], name='role_constraint'),
        sa.ForeignKeyConstraint(['user_id'], ['user.id'], name='user_constraint'),
    )
    op.create_table(
        'post',
        sa.Column('id', sa.Integer(), primary_key=True),
        sa.Column('title', sa.String(length=255)),
        sa.Column('slug', sa.String(length=255)),
        sa.Column('content', sa.Text()),
        sa.Column('published', sa.Boolean()),
        sa.Column('timestamp', sa.DateTime()),
        sa.Column('header_image_id', sa.Integer()),
        sa.Column('tagline', sa.String(length=5000)),
        sa.Column('author_id', sa.Integer(), sa.ForeignKey('user.id'), nullable=False),
        sa.PrimaryKeyConstraint('id', name='post_primary_key'),
        sa.UniqueConstraint('slug', name='slug_unique_constraint'),
        sa.UniqueConstraint('title', name='title_unique_constraint'),
    )
    op.create_table(
        'image',
        sa.Column('id', sa.Integer(), primary_key=True),
        sa.Column('url', sa.String(length=255), nullable=False),
        sa.Column('post_id', sa.Integer(), sa.ForeignKey('post.id')),
    )
    op.create_table(
        'comment',
        sa.Column('id', sa.Integer(), primary_key=True),
        sa.Column('user_id', sa.Integer(), sa.ForeignKey('user.id'), nullable=False),
        sa.Column('post_id', sa.Integer(), sa.ForeignKey('post.id'), nullable=False),
        sa.Column('text', sa.Text(), nullable=True),
        sa.Column('timestamp', sa.DateTime(), nullable=False),
    )
    ### end Alembic commands ###


def downgrade():
    op.drop_table('comment')
    op.drop_table('image')
    op.drop_table('post')
    op.drop_table('roles_user')
    op.drop_table('user')
    op.drop_table('role')
    ### end Alembic commands ###
