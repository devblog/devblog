import jwt
import pytest
from bcrypt import checkpw
from flask_jwt_extended import jwt_required
from sqlalchemy.exc import IntegrityError

from devblog.api_security import User, admin_required
from devblog.responses import make_response
from tests.conftest import login
from tests.factories import UserFactory


def test_handles_byes_password(_session):
    user = User(alias="name", email="some@ex.fake", password=b"abcdefg")
    _session.add(user)
    _session.commit()
    assert checkpw(
        b"abcdefg", user.password.encode("utf-8")  # pylint: disable=no-member
    )


def test_require_password(_session):
    _session.add(User(alias="name", email="some@ex.fake"))
    with pytest.raises(IntegrityError):
        _session.commit()


def test_login(client, _user):
    json = {"email": _user.email, "password": "somepassword"}
    response = client.post("/api/login", json=json)
    assert response.status_code == 200


def test_login_requires_json(client):
    response = client.post("/api/login", data=b"blah")
    assert response.status_code == 400


def test_requires_parameters(client):
    response = client.post("/api/login", json={"email": "user@example.fake"})
    assert response.status_code == 400
    response = client.post("/api/login", json={"password": "faker"})
    assert response.status_code == 400


def test_bad_password(client, _user):
    json = {"email": _user.email, "password": "baddpass"}
    response = client.post("/api/login", json=json)
    assert response.status_code == 401


def test_bad_email(client, _user):
    json = {"email": "user@ele.fake", "password": "somepassword"}
    response = client.post("/api/login", json=json)
    assert response.status_code == 401


def test_payload(_app, client, _user):
    json = {"email": _user.email, "password": "somepassword"}
    response = client.post("/api/login", json=json)
    assert response.status_code == 200
    token = response.json["data"]["access_token"]
    decoded = jwt.decode(token, key=_app.config["JWT_PUBLIC_KEY"], algorithms=["RS512"])
    assert decoded["sub"] == _user.email
    assert decoded["roles"] == ["admin"]


def test_register(client, _session):
    json = {
        "alias": "somename",
        "email": "user@example.fake",
        "password": "somepassword",
    }
    response = client.post("/api/register", json=json)
    assert response.status_code == 200
    token = response.json["data"]["access_token"]
    headers = {"Authorization": f"Bearer {token}"}
    response = client.get("/api/user", headers=headers)
    assert response.status_code == 200


def test_validation_error(client, _session):
    json = {"alias": "somename", "email": "notanemail", "password": "somepassword"}
    response = client.post("/api/register", json=json)
    assert response.status_code == 400
    assert "email" in response.json["error"]["message"]


def test_user_endpoint(client, _user):
    json = {"email": _user.email, "password": "somepassword"}
    response = client.post("/api/login", json=json)
    assert response.status_code == 200
    token = response.json["data"]["access_token"]
    headers = {"Authorization": f"Bearer {token}"}
    response = client.get("/api/user", headers=headers)
    assert response.status_code == 200
    user_data = response.json["data"]["user"]
    assert user_data["alias"] == _user.alias
    assert "password" not in user_data


def test_protected(_app, client, _user):
    @_app.route("/protected")
    @jwt_required()
    def _fakeroute():
        return make_response(status="good")

    logged_in = login(_user.email, client)
    response = client.get("/protected")
    assert response.status_code == 401
    response = client.get("/protected", headers=logged_in)
    assert response.status_code == 200


def test_admin_required(_app, client, _session, _user):
    non_admin = UserFactory()
    _session.add(non_admin)
    _session.commit()

    @_app.route("/regular")
    @jwt_required()
    def _regular():
        return make_response(status="good")

    @_app.route("/admin")
    @admin_required
    def _fakeroute():
        return make_response(status="good")

    logged_in = login(_user.email, client)
    other_headers = login(non_admin.email, client)
    response = client.get("/regular", headers=other_headers)
    assert response.status_code == 200
    response = client.get("/admin", headers=other_headers)
    assert response.status_code == 403
    response = client.get("/admin", headers=logged_in)
    assert response.status_code == 200
