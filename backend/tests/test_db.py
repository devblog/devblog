import pytest
from sqlalchemy.exc import IntegrityError
from sqlalchemy.orm.exc import NoResultFound

from devblog.api_security.models import User
from tests.factories import UserFactory


def test_connection(_session):
    created = UserFactory()
    _session.add(created)
    _session.commit()
    user = _session.query(User).one()
    assert user.alias == created.alias
    assert user.password != b"somepassword"


def test_session_manager_commits_roll_back(_session):
    with pytest.raises(NoResultFound):
        _session.query(User).one()


def test_session_manager_commits(_app, _session):
    user = UserFactory()
    alias = user.alias
    _session.add(user)
    user = _session.query(User).one()
    assert user.alias == alias
    assert user.password != b"somepassword"


def test_session_manager_can_fail(_app, _session):
    with pytest.raises(IntegrityError):
        _session.add(UserFactory(__sequence=1))
        _session.add(UserFactory(__sequence=1))
        _session.commit()
    _session.rollback()
    with pytest.raises(NoResultFound):
        _session.query(User).one()
