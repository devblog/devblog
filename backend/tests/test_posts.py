from datetime import datetime, timedelta
from tempfile import TemporaryFile

import pytest
from botocore.stub import ANY

from tests.conftest import login
from tests.factories import CommentFactory, ImageFactory, PostFactory, UserFactory


@pytest.fixture(name="_posts")
def posts(_user, _session):
    post = PostFactory(author=_user)
    _session.add(post)
    _session.commit()
    return [post]


@pytest.fixture(name="_drafts")
def drafts(_user, _session):
    post = PostFactory(author=_user, published=False)
    post2 = PostFactory(author=_user, published=False)
    _session.add_all([post, post2])
    _session.commit()
    return [post, post2]


def test_slug(_session):
    post = PostFactory()
    with pytest.raises(AttributeError):
        post.slug = "hi"

    assert post.slug.startswith("article-")
    post.title = "Other Title"
    assert post.slug.startswith("article-")


def test_get_posts(client, _posts):
    title = _posts[0].title
    response = client.get("/api/blog/post")
    assert response.status_code == 200
    returned_posts = response.json["data"]["posts"]
    assert len(returned_posts) == 1
    assert returned_posts[0]["title"] == title


def test_admin_gets_all_drafts(client, _session, _logged_in, _posts, _drafts):
    _user = UserFactory()
    draft3 = PostFactory(title="extra", author=_user, published=False)
    _session.add_all([_user, draft3])
    _session.commit()
    titles = [draft.title for draft in _drafts] + ["extra"]
    response = client.get("/api/blog/post?drafts=true", headers=_logged_in)
    assert response.status_code == 200
    returned_posts = response.json["data"]["posts"]
    assert len(returned_posts) == 3
    assert [post["title"] for post in returned_posts] == titles


def test_non_admin_doesnt_see_all_drafts(client, _session, _posts, _drafts):
    _user = UserFactory()
    draft3 = PostFactory(title="extra", author=_user, published=False)
    _session.add_all([_user, draft3])
    _session.commit()
    headers = login(_user.email, client)
    response = client.get("/api/blog/post?drafts=true", headers=headers)
    assert response.status_code == 200
    assert response.json["data"]["posts"][0]["title"] == "extra"


def test_anon_sees_no_drafts(client, _drafts):
    response = client.get("/api/blog/post?drafts=true")
    assert response.status_code == 403


def test_get_post(client, _posts):
    slug, title = _posts[0].slug, _posts[0].title
    response = client.get(f"/api/blog/post/{slug}")
    assert response.status_code == 200
    assert response.json["data"]["post"]["title"] == title


def test_admin_get_draft(client, _user, _drafts):
    slug = _drafts[0].slug
    response = client.get(f"/api/blog/post/{slug}", headers=login(_user.email, client))
    assert response.status_code == 200


def test_author_get_draft(client, _session):
    other_user = UserFactory()
    post = PostFactory(author=other_user, published=False)
    _session.add(other_user)
    _session.add(post)
    _session.commit()

    response = client.get(
        f"/api/blog/post/{post.slug}", headers=login(other_user.email, client)
    )
    assert response.status_code == 200


def test_public_cant_get_draft(client, _user, _session):
    other_user = UserFactory()
    post = PostFactory(author=_user, published=False)
    _session.add(other_user)
    _session.add(post)
    _session.commit()
    response = client.get(
        f"/api/blog/post/{post.slug}", headers=login(other_user.email, client)
    )
    assert response.status_code == 403


def test_create_post(client, _user, _session):
    image = ImageFactory()
    _session.add(image)
    _session.commit()
    data = {
        "post": {
            "title": "my post",
            "tagline": "is awesome",
            "published": True,
            "header_image": {"id": image.id},
        }
    }
    response = client.post(
        "/api/blog/post", json=data, headers=login(_user.email, client)
    )
    assert response.status_code == 200
    post = response.json["data"]["post"]
    assert post["title"] == "my post"
    assert post["slug"] == "my-post"
    assert post["author"]["id"] == _user.id
    assert post["header_image"]["url"] == image.url
    response = client.get("/api/blog/post/my-post")
    assert response.status_code == 200


def test_create_post_with_images(client, _user, _session):
    image = ImageFactory()
    _session.add(image)
    _session.commit()
    data = {
        "post": {
            "title": "my post",
            "tagline": "is awesome",
            "published": True,
            "images": [{"id": image.id}],
        }
    }
    response = client.post(
        "/api/blog/post", json=data, headers=login(_user.email, client)
    )
    assert response.status_code == 200
    post = response.json["data"]["post"]
    assert post["images"][0]["url"] == image.url


def test_cant_get_draft_anon(client, _drafts):
    response = client.get(f"/api/blog/post/{_drafts[0].slug}")
    assert response.status_code == 403


def test_update_post(client, _drafts, _user):
    data = {"post": {"published": True}}
    response = client.put(
        f"/api/blog/post/{_drafts[0].slug}",
        json=data,
        headers=login(_user.email, client),
    )
    assert response.status_code == 200
    response = client.get(f"/api/blog/post/{_drafts[0].slug}")
    assert response.status_code == 200


def test_update_post_images(client, _session, _user, _logged_in):
    post = PostFactory(author=_user)
    old_image = ImageFactory(post=post)
    new_image = ImageFactory(post=None)
    _session.add_all([post, old_image, new_image])
    _session.commit()
    response = client.get(f"/api/blog/post/{post.slug}")
    assert response.status_code == 200
    assert response.json["data"]["post"]["images"][0]["url"] == old_image.url
    data = {"post": {"images": [{"id": new_image.id}]}}
    response = client.put(
        f"/api/blog/post/{post.slug}", json=data, headers=login(_user.email, client)
    )
    assert response.status_code == 200
    assert response.json["data"]["post"]["images"][0]["url"] == new_image.url
    assert len(response.json["data"]["post"]["images"]) == 1


def test_delete_post(client, _posts, _logged_in):
    response = client.delete(f"/api/blog/post/{_posts[0].slug}", headers=_logged_in)
    assert response.status_code == 200
    response = client.get(f"/api/blog/post/{_posts[0].slug}")
    assert response.status_code == 404


def test_change_image(client, _session, _user, _logged_in):
    image1 = ImageFactory()
    image2 = ImageFactory()
    post = PostFactory(header_image=image1, author=_user)
    _session.add_all([image1, image2, post])
    _session.commit()

    data = {"post": {"header_image": {"id": image2.id}}}
    response = client.put(f"/api/blog/post/{post.slug}", json=data, headers=_logged_in)
    assert response.status_code == 200
    response = client.get(f"/api/blog/post/{post.slug}")
    assert response.status_code == 200
    assert response.json["data"]["post"]["header_image"]["url"] == image2.url


def test_upload_image(client, _app, _logged_in, _s3):
    bucket = _app.config["S3_BUCKET"]
    folder = _app.config["S3_UPLOAD_DIRECTORY"]
    _s3.add_response(
        "put_object",
        {},
        {
            "Body": b"abcdefg1234",
            "Bucket": _app.config["S3_BUCKET"],
            "Key": ANY,
            "ACL": "public-read",
        },
    )

    with TemporaryFile() as f:
        f.write(b"abcdefg1234")
        f.seek(0)
        response = client.post(
            "/api/blog/image", data={"file": (f, "filename.png")}, headers=_logged_in
        )
    assert response.status_code == 200
    url = response.json["data"]["image"]["url"]
    assert url.startswith(f"https://{bucket}.s3.amazonaws.com/{folder}/")
    assert url.endswith(".png")


def test_fetch_comments(client, _posts, _session, _user):
    comment = CommentFactory(user=_user, post=_posts[0], text="first but second")
    comment2 = CommentFactory(
        user=_user,
        post=_posts[0],
        text="should be first",
        timestamp=datetime.utcnow() - timedelta(days=5),
    )
    _session.add_all([comment, comment2])
    _session.commit()
    response = client.get(f"/api/blog/comment/{_posts[0].slug}")
    assert response.status_code == 200
    comments = [c["text"] for c in response.json["data"]["comments"]]
    assert comments == ["should be first", "first but second"]


def test_add_comment(client, _posts, _session, _user, _logged_in):
    response = client.post(
        f"/api/blog/comment/{_posts[0].slug}",
        json={"text": "this post sucks"},
        headers=_logged_in,
    )
    assert response.status_code == 200
    response = client.get(f"/api/blog/comment/{_posts[0].slug}")
    assert response.status_code == 200
    comment = response.json["data"]["comments"][0]
    assert comment["text"] == "this post sucks"
    assert comment["username"] == _user.alias


def test_anon_cant_add_comment(client, _posts, _session, _logged_in):
    response = client.post(
        f"/api/blog/comment/{_posts[0].slug}", json={"text": "this post sucks"}
    )
    assert response.status_code == 401


def test_bad_image_upload_fails(client, _logged_in):
    response = client.post("/api/blog/image", headers=_logged_in)
    assert response.status_code == 400
