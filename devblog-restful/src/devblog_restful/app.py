from fastapi import FastAPI
from fastapi.responses import HTMLResponse
from fastui import prebuilt_html
from fastapi.responses import PlainTextResponse

from devblog_restful.resources import users
from devblog_restful.site import main, about
from devblog_restful.db import create_db_and_tables

app = FastAPI()


app.include_router(users.router, prefix="/api/users")
app.include_router(about.router, prefix="/api/about")
app.include_router(main.router, prefix="/api")


@app.get('/robots.txt', response_class=PlainTextResponse)
async def robots_txt() -> str:
    return 'User-agent: *\nAllow: /'


@app.get('/favicon.ico', status_code=404, response_class=PlainTextResponse)
async def favicon_ico() -> str:
    return 'page not found'


@app.get('/{path:path}')
async def html_landing() -> HTMLResponse:
    return HTMLResponse(prebuilt_html(title='FastUI Demo'))


@app.on_event("startup")
def on_startup():
    create_db_and_tables()
