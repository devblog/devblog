from __future__ import annotations as _annotations

from fastui import AnyComponent
from fastui import components as c
from fastui.events import GoToEvent


def home_page(*components: AnyComponent, title: str | None = None) -> list[AnyComponent]:
    return [
        c.PageTitle(text=f'Paul Becotte — {title}' if title else 'Paul Becotte'),
        c.Navbar(
            title='Paul Becotte',
            title_event=GoToEvent(url='/'),
            end_links=[
                c.Link(
                    components=[c.Text(text='Home')],
                    on_click=GoToEvent(url='/posts'),
                    active='startswith:/posts',
                ),
                c.Link(
                    components=[c.Text(text='About Me')],
                    on_click=GoToEvent(url='/about'),
                    active='startswith:/about',
                ),
                c.Link(
                    components=[c.Text(text='Sign Up')],
                    on_click=GoToEvent(url='/users/signup'),
                    active='startswith:/users/signup',
                ),
                c.Link(
                    components=[c.Text(text='Log In')],
                    on_click=GoToEvent(url='/users/login'),
                    active='startswith:/users/login',
                ),
            ],
        ),
        c.Page(
            components=[
                *((c.Heading(text=title),) if title else ()),
                *components,
            ],
        ),
        c.Footer(
            extra_text='FastUI Demo',
            links=[
                c.Link(
                    components=[c.Text(text='Github')], on_click=GoToEvent(url='https://github.com/pydantic/FastUI')
                ),
                c.Link(components=[c.Text(text='PyPI')], on_click=GoToEvent(url='https://pypi.org/project/fastui/')),
                c.Link(components=[c.Text(text='NPM')], on_click=GoToEvent(url='https://www.npmjs.com/org/pydantic/')),
            ],
        ),
    ]
