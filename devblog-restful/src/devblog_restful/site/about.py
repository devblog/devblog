from textwrap import dedent

from fastapi import APIRouter
from fastui import AnyComponent, FastUI
from fastui import components as c

from .shared import home_page

router = APIRouter()


@router.get('', response_model=FastUI, response_model_exclude_none=True)
def about() -> list[AnyComponent]:
    # language=markdown
    markdown = dedent("""
        I am an obsessive technologist who lives to build distributed systems
        that are fault tolerant, can scale horizontally, and provide easy 
        operations and development. I prefer to build something and try it 
        out versus imagining I can perfect it in a vacuum. I believe that 
        investments in work processes have an immediate positive return on 
        investment. I am at my best when solving new problems that require 
        integrating a bunch of tools that were never meant to work that way.

        My career so far has included creating DevOps at a hardware company 
        that didn't believe it was possible, going from developer to CTO at 
        an ad-tech startup, and working on automation, tooling, and the Cloud 
        at a several financial companies. I haven’t yet met the problem that 
        didn’t interest me.

        I have extensive expertise in Python application development, especially 
        testing and SqlAlchemy. I am also an expert in building on the Cloud 
        using Terraform and configuration as code. Finally, I can describe 
        myself as an expert on Kubernetes, both using it and deploying apps on 
        it and setting it up and administering it. I have lesser but functional 
        experience with Javascript in both React and Angular variants, data 
        pipelines and storage, and more traditional sysadmin tasks.
    """)

    return home_page(
        c.Heading(text="Hi! I'm Paul Becotte", level=1),
        c.Markdown(text=markdown)
    )


@router.get('/{path:path}', status_code=404)
async def api_404():
    # so we don't fall through to the index page
    return {'message': 'Not Found'}
