from sqlmodel import create_engine, Session, SQLModel

from devblog_restful.resources.users.models import User

sqlite_file_name = "database.db"
sqlite_url = f"sqlite:///{sqlite_file_name}"

connect_args = {"check_same_thread": False}
engine = create_engine(sqlite_url, echo=True, connect_args=connect_args, future=True)


def get_session():  # pragma: no cover
    with Session(engine, future=True) as session:
        yield session

def create_db_and_tables():
    SQLModel.metadata.create_all(engine)
