import jwt
from datetime import datetime, timedelta, UTC
from pathlib import Path

from cryptography.hazmat.backends import default_backend
from cryptography.hazmat.primitives import serialization
from cryptography.hazmat.primitives.asymmetric import rsa
from fastapi import Depends, HTTPException
from fastapi.security import OAuth2PasswordBearer
from pydantic import BaseModel

from devblog_restful.config import JWT_PRIVATE_KEY, JWT_ALGORITHM


oauth2_scheme = OAuth2PasswordBearer(tokenUrl="/users/login")


class Token(BaseModel):
    access_token: str
    token_type: str


class TokenData(BaseModel):
    id: int
    roles: list[str]


def generate_keys(key_path):
    private_key = rsa.generate_private_key(
        public_exponent=65537, key_size=2048, backend=default_backend()
    )
    pem = private_key.private_bytes(
        encoding=serialization.Encoding.PEM,
        format=serialization.PrivateFormat.PKCS8,
        encryption_algorithm=serialization.NoEncryption(),
    )
    public_key = private_key.public_key()
    public_pem = public_key.public_bytes(
        encoding=serialization.Encoding.PEM,
        format=serialization.PublicFormat.SubjectPublicKeyInfo,
    )
    folder = Path(key_path)
    folder.mkdir(exist_ok=True)
    (folder / "public_key.pem").write_bytes(public_pem)
    (folder / "private_key.pem").write_bytes(pem)


def create_access_token(user: TokenData):
    expire = datetime.now(UTC) + timedelta(days=30)
    data = {
        "sub": user.id,
        "roles": ",".join(user.roles),
        "exp": expire
    }
    encoded_jwt = jwt.encode(data, JWT_PRIVATE_KEY, algorithm=JWT_ALGORITHM)
    return encoded_jwt


# async def get_current_user(token: str = Depends(oauth2_scheme)) -> UserSchema:
#     credentials_exception = HTTPException(
#         status_code=status.HTTP_401_UNAUTHORIZED,
#         detail="Could not validate credentials",
#         headers={"WWW-Authenticate": "Bearer"},
#     )
#     try:
#         payload = jwt.decode(token, SECRET_KEY, algorithms=[ALGORITHM])
#         user: dict = json.loads(payload.get("sub", ""))
#         if not user:
#             raise credentials_exception
#         return UserSchema(**user)
#     except JWTError as e:
#         raise credentials_exception from e
#
#
# async def get_current_active_user(current_user: UserSchema = Depends(get_current_user)):
#     if not current_user.active:
#         raise HTTPException(status_code=400, detail="Inactive user")
#     return current_user



if __name__ == "__main__":
    generate_keys("insecure_keys")
