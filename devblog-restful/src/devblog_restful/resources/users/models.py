from datetime import datetime, UTC
from typing import Annotated, Self

import jwt
from bcrypt import gensalt, hashpw
from sqlmodel import SQLModel, Field, Relationship
from fastapi import Header, HTTPException
from fastui.auth import AuthRedirect

from devblog_restful.config import JWT_PRIVATE_KEY, JWT_ALGORITHM
from devblog_restful.resources.roles import Role, RoleUser


class UserBase(SQLModel):
    alias: str
    email: str
    hashed_password: str
    active: bool = True
    confirmed_at: datetime = Field(default_factory=lambda: datetime.now(UTC))

    @classmethod
    def from_request(cls, authorization: Annotated[str, Header()] = '') -> Self:
        user = cls.from_request_opt(authorization)
        if user is None:
            raise AuthRedirect('/auth/login/password')
        else:
            return user

    @classmethod
    def from_request_opt(cls, authorization: Annotated[str, Header()] = '') -> Self | None:
        try:
            token = authorization.split(' ', 1)[1]
        except IndexError:
            return None

        try:
            payload = jwt.decode(token, JWT_PRIVATE_KEY, algorithm=JWT_ALGORITHM)
        except jwt.ExpiredSignatureError:
            return None
        except jwt.DecodeError:
            raise HTTPException(status_code=401, detail='Invalid token')
        else:
            # existing token might not have 'exp' field
            payload.pop('exp', None)
            return cls(**payload)

class User(UserBase, table=True):
    id: int | None = Field(default=None, primary_key=True)
    roles: list[Role] = Relationship(back_populates="users", link_model=RoleUser)

    @staticmethod
    def hash_password(password: str) -> str:
        rounds = 5
        if not isinstance(password, bytes):
            password = password.encode("utf-8")
        return hashpw(password, gensalt(rounds)).decode("utf-8")


class UserRead(SQLModel):
    id: int
    alias: str
    email: str
    active: bool
    confirmed_at: datetime
    roles: list[Role]


class UserCreate(SQLModel):
    alias: str
    email: str
    password: str
