from typing import Annotated

from bcrypt._bcrypt import checkpw
from fastapi import HTTPException
from fastui import FastUI, AnyComponent, components as c
from fastui.components.display import DisplayLookup
from fastui.events import GoToEvent, BackEvent, AuthEvent, PageEvent
from fastui.forms import fastui_form
from fastapi import APIRouter, Depends
from pydantic import BaseModel, EmailStr, Field, SecretStr
from sqlmodel import Session, select

from devblog_restful.db import get_session
from devblog_restful.site.shared import home_page
from .models import User, UserCreate, UserRead
from .tokens import create_access_token, TokenData

router = APIRouter()




@router.get("/signup", response_model=FastUI, response_model_exclude_none=True)
def signup_form() -> list[AnyComponent]:
    return home_page(
        c.Heading(text="Sign Up", level=2),
        c.ModelForm(model=UserCreate, submit_url="/api/users", display_mode="page")
    )


class LoginForm(BaseModel):
    email: EmailStr = Field(
        title='Email Address', json_schema_extra={'autocomplete': 'email'}
    )
    password: SecretStr = Field(
        title='Password',
        json_schema_extra={'autocomplete': 'current-password'},
    )

@router.get("/login", response_model=FastUI, response_model_exclude_none=True)
def login_form() -> list[AnyComponent]:
    return home_page(
        c.Heading(text="Login", level=2),
        c.ModelForm(model=LoginForm, submit_url="/api/users/login", display_mode="page"),
    )


@router.post("/login", response_model=FastUI, response_model_exclude_none=True)
def login(form: Annotated[LoginForm, fastui_form(LoginForm)], session: Session = Depends(get_session)) -> list[AnyComponent]:
    q = select(User).where(User.email == form.email)
    user = session.exec(q).one_or_none()
    if not user or not checkpw(form.password.get_secret_value().encode("utf-8"), user.hashed_password.encode("utf-8")):
        return [c.FireEvent(message="Login Failed", event=AuthEvent(token=False, url=f"/users/login"))]
    token = create_access_token(TokenData(id=user.id, roles=[role.name for role in user.roles]))
    return [c.FireEvent(event=AuthEvent(token=token, url=f"/users/{user.id}"))]

@router.get("/{user_id}", response_model=FastUI, response_model_exclude_none=True)
def user_profile(user_id: int, session: Session = Depends(get_session)) -> list[AnyComponent]:
    try:
        q = select(User).where(User.id == user_id)
        user = session.exec(q).one_or_none()
    except StopIteration:
        raise HTTPException(status_code=404, detail="User not found")
    return home_page(
            c.Heading(text=user.alias, level=2),
            c.Link(components=[c.Text(text='Back')], on_click=BackEvent()),
            c.Details(data=UserRead.model_validate(user)),
        )


@router.get("", response_model=FastUI, response_model_exclude_none=True)
def users_table(session: Session = Depends(get_session)) -> list[AnyComponent]:
    q = select(User)
    users = session.exec(q).all()
    return home_page(
            c.Heading(text='Users', level=2),  # renders `<h2>Users</h2>`
            c.Table(
                data=users,
                data_model=UserRead,
                no_data_message="No users found",
                columns=[
                    # the first is the users, name rendered as a link to their profile
                    DisplayLookup(field='alias', on_click=GoToEvent(url='/users/{id}')),
                    DisplayLookup(field="email"),
                    DisplayLookup(field="active"),
                    DisplayLookup(field="confirmed_at"),
                ],
            ),
        )


@router.post("", response_model=FastUI, response_model_exclude_none=True)
def user_create(user_data: Annotated[UserCreate, fastui_form(UserCreate)], session: Session = Depends(get_session)) -> list[AnyComponent]:
    user = User.model_validate(user_data, update={"hashed_password": User.hash_password(user_data.password)})
    session.add(user)
    session.commit()
    session.refresh(user)
    token = create_access_token(TokenData(id=user.id, roles=[role.name for role in user.roles]))
    return [c.FireEvent(event=AuthEvent(token=token, url=f"/users/{user.id}"))]
