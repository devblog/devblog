from sqlmodel import SQLModel, Field, Relationship
from typing import TYPE_CHECKING

if TYPE_CHECKING:
    from devblog_restful.resources.users import User

class RoleUser(SQLModel, table=True):
    user_id: int | None = Field(default=None, foreign_key="user.id", primary_key=True)
    role_id: int | None = Field(default=None, foreign_key="role.id", primary_key=True)


class Role(SQLModel, table=True):
    id: int = Field(default=None, primary_key=True)
    name: str
    description: str

    users: list["User"] = Relationship(back_populates="roles", link_model=RoleUser)
