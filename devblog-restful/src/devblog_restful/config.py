import os
from pathlib import Path

JWT_ALGORITHM = "RS512"
key_path = os.environ.get("KEY_PATH", "insecure_keys")
try:
    JWT_PUBLIC_KEY = (Path(key_path) / "public_key.pem").read_text()
    JWT_PRIVATE_KEY = (Path(key_path) / "private_key.pem").read_text()
except FileNotFoundError:
    JWT_PUBLIC_KEY = ""
    JWT_PRIVATE_KEY = ""
    print(f"Could not load jwt tokens from {key_path}")
BCRYPT_ROUNDS = 5
